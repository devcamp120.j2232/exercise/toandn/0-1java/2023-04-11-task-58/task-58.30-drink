package com.devcamp.dbdrinks.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.dbdrinks.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
