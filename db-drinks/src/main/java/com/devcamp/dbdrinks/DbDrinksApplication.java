package com.devcamp.dbdrinks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbDrinksApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbDrinksApplication.class, args);
	}

}
